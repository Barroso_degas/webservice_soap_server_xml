package br.com.process.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ProcessXmlService {

	@WebMethod(operationName="processXmlAsString")
	public boolean processXmlAsString( @WebParam(name="xmlString") String xmlString );
	
	@WebMethod(operationName="processXmlAsByteArray")
	public boolean processXmlAsByteArray( @WebParam(name="xmlByteArray") byte[] xmlByteArray );
	
}
