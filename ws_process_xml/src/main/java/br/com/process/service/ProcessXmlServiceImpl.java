package br.com.process.service;

import java.io.File;
import java.io.FileOutputStream;

import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import br.com.process.xmlModel.Teste;

@WebService(endpointInterface="br.com.process.service.ProcessXmlService")
public class ProcessXmlServiceImpl implements ProcessXmlService {

	@Override
	public boolean processXmlAsString(String xmlString) {
		
		if( xmlString.equals( "Teste" ) ) {
			return true;
		}
		return false;
	}

	@Override
	public boolean processXmlAsByteArray(byte[] xmlByteArray) {
		
		Teste teste = null;
		
		try {
			
			File file = new java.io.File("c:/Temp/teste.xml");  
			FileOutputStream in = new FileOutputStream(file) ;    
			in.write(xmlByteArray);  
			in.close();
			
			JAXBContext context = JAXBContext.newInstance(Teste.class);
            Unmarshaller un = context.createUnmarshaller();
            teste = (Teste) un.unmarshal(new File("c:/Temp/teste.xml"));			
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if( teste != null && teste.getInfo().trim().equals("teste") ) {
			return true;
		}
		return false;
	}
}
