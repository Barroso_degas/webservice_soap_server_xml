package br.com.process.xmlModel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="teste")
public class Teste {

	@XmlElement(name = "info", required = true, nillable = false, type = String.class)
	private String info;

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Teste: " + info;
	}
}
